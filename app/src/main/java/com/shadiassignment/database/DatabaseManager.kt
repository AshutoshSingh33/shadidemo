package com.shadiassignment.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.shadiassignment.database.dao.MatchResultDao
import com.shadiassignment.database.model.Converters
import com.shadiassignment.database.model.ResultEntity
import com.shadiassignment.network.mapper.ResultDTOMapper

@Database(entities = [ResultEntity::class], version = 8)
@TypeConverters(Converters::class)
abstract class DatabaseManager : RoomDatabase() {

    companion object {
        val DATABASE_NAME: String = "shadi_db"
    }

    abstract fun matchResultDao(): MatchResultDao
}