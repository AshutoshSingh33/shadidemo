package com.shadiassignment.database.dao

import androidx.room.*
import com.shadiassignment.database.model.ResultEntity

@Dao
interface MatchResultDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(resultEntity: List<ResultEntity>)

    @Query("SELECT * FROM MatchResult")
    suspend fun getAllMatch(): List<ResultEntity>

    @Query("DELETE FROM MatchResult")
    suspend fun deleteTable()

    @Update
    suspend fun update(resultEntity: ResultEntity)

    @Query("SELECT * FROM MatchResult WHERE cell =:cell")
    suspend fun getQuestion(cell: String): ResultEntity

}