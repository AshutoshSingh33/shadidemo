package com.shadiassignment.database.mapper

import com.shadiassignment.database.model.ResultEntity
import com.shadiassignment.domain.Result
import com.shadiassignment.network.model.ResultDTO
import com.shadiassignment.presentation.match_result.ActionStatus
import com.shadiassignment.presentation.match_result.getActionStatus
import com.shadiassignment.util.DomainMapper

class ResultMapper : DomainMapper<ResultEntity, Result> {

    override fun mapFromDomainModel(domainModel: Result): ResultEntity {
        TODO("Not yet implemented")
    }

    override fun mapToDomainModel(model: ResultEntity): Result {
        return Result(
            cell = model.cell,
            name = model.name.first + model.name.last,
            age = model.dobDTO.age,
            city = model.locationDTO.city,
            state = model.locationDTO.state,
            image = model.pictureDTO.large,
            actionStatus = getActionStatus(model.actionStatus)?: ActionStatus.NONE
        )
    }

    fun toDomainList(initial: List<ResultEntity>): List<Result> {
        return initial.map { mapToDomainModel(it) }
    }

}