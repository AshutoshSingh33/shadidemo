package com.shadiassignment.database.model

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.shadiassignment.network.model.*

@Entity(tableName = "MatchResult")
data class ResultEntity(

    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "cell")
    var cell: String,

    @ColumnInfo(name = "phone")
    var phone: String,

    @ColumnInfo(name = "gender")
    var gender: String,

    @Embedded
    var name: NameDTO,

    @Embedded
    var locationDTO: LocationDTO,

    @ColumnInfo(name ="email")
    var email: String,

    @Embedded
    var loginDTO: LoginDTO,

    @Embedded
    var dobDTO: DobDTO,

    @Embedded
    var idDTO: IdDTO,

    @Embedded
    var pictureDTO: PictureDTO,

    @ColumnInfo(name = "nat")
    var nat: String,

    @ColumnInfo(name = "actionStatus")
    var actionStatus: Int

)