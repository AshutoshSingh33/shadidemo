package com.shadiassignment.database.model

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.shadiassignment.network.model.CoordinateDTO
import com.shadiassignment.network.model.StreetDTO
import com.shadiassignment.network.model.TimeZoneDTO
import java.lang.reflect.Type

class Converters {

    @TypeConverter
    fun fromStringToStreet(value: String): StreetDTO{
        return Gson().fromJson(value, StreetDTO::class.java)
    }

    @TypeConverter
    fun fromStreetToString(streetDTO: StreetDTO): String? {
        val gson = Gson()
        return gson.toJson(streetDTO)
    }

    @TypeConverter
    fun fromStringToCoOrdinate(value: String): CoordinateDTO{
        return Gson().fromJson(value, CoordinateDTO::class.java)
    }

    @TypeConverter
    fun fromCoOrdinateToString(coordinateDTO: CoordinateDTO): String? {
        val gson = Gson()
        return gson.toJson(coordinateDTO)
    }

    @TypeConverter
    fun fromStringToTimeZone(value: String): TimeZoneDTO{
        return Gson().fromJson(value, TimeZoneDTO::class.java)
    }

    @TypeConverter
    fun fromCoOrdinateToString(timezoneDTO: TimeZoneDTO): String? {
        val gson = Gson()
        return gson.toJson(timezoneDTO)
    }
}