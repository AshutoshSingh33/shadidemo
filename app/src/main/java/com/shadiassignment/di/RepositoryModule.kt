package com.shadiassignment.di

import com.shadiassignment.database.dao.MatchResultDao
import com.shadiassignment.database.mapper.ResultMapper
import com.shadiassignment.network.RetrofitService
import com.shadiassignment.network.mapper.ResultDTOMapper
import com.shadiassignment.repository.MatchResultRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object RepositoryModule {

    @Singleton
    @Provides
    fun provideMathResultRepository(retrofitService: RetrofitService, dtoMapper: ResultDTOMapper, dao: MatchResultDao, resultMapper: ResultMapper) : MatchResultRepository {
        return MatchResultRepository(retrofitService, dtoMapper, dao, resultMapper)
    }

}