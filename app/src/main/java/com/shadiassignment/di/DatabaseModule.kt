package com.shadiassignment.di

import android.content.Context
import androidx.room.Room
import com.shadiassignment.database.DatabaseManager
import com.shadiassignment.database.dao.MatchResultDao
import com.shadiassignment.database.mapper.ResultMapper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object DatabaseModule {

    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext context: Context): DatabaseManager {
        return Room
            .databaseBuilder(
                context,
                DatabaseManager::class.java,
                DatabaseManager.DATABASE_NAME
            )
            .fallbackToDestructiveMigration()
            .build()
    }

    @Provides
    @Singleton
    fun provideMatchResultDao(databaseManager: DatabaseManager): MatchResultDao {
        return databaseManager.matchResultDao()
    }

    @Singleton
    @Provides
    fun provideResultMapper() : ResultMapper {
        return ResultMapper()
    }
}