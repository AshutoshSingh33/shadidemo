package com.shadiassignment.domain

import com.shadiassignment.presentation.match_result.ActionStatus

data class Result (
    val cell: String,
    val name: String,
    val age: Int,
    var city: String,
    var state: String,
    val image : String,
    val actionStatus: ActionStatus
)