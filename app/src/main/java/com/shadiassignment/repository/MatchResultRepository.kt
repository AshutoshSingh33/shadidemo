package com.shadiassignment.repository

import com.shadiassignment.database.dao.MatchResultDao
import com.shadiassignment.database.mapper.ResultMapper
import com.shadiassignment.database.model.ResultEntity
import com.shadiassignment.domain.Result
import com.shadiassignment.network.RetrofitService
import com.shadiassignment.network.mapper.ResultDTOMapper
import com.shadiassignment.network.model.MatchResponseDTO
import com.shadiassignment.network.model.ResultDTO
import com.shadiassignment.presentation.match_result.ActionStatus

class MatchResultRepository(
    private val retrofitService: RetrofitService,
    private val mapper: ResultDTOMapper,
    private val dao: MatchResultDao,
    private val entityMapper: ResultMapper
) {

    suspend fun getAllMatch(count: String): MatchResponseDTO {
        return retrofitService.getMatchResult(count)
    }

    suspend fun mapToResultEntity(models: ArrayList<ResultDTO>): List<ResultEntity> {
        val resultEntities = mapper.toDomainList(models)
        dao.deleteTable()
        dao.insert(resultEntities)
        return dao.getAllMatch()
    }

    suspend fun mapToResultDomainList(entities: List<ResultEntity>): ArrayList<Result> {
        return entityMapper.toDomainList(entities) as ArrayList<Result>
    }

    suspend fun getAllResultEntities(): List<ResultEntity> {
        return dao.getAllMatch()
    }

    suspend fun updateResultEntity(cell: String, actionStatus: ActionStatus): ResultEntity {
        val resultEntity = dao.getQuestion(cell)
        val updatedResultEntity = resultEntity.copy(actionStatus = actionStatus.value)
        dao.update(updatedResultEntity)
        return dao.getQuestion(cell)
    }

    suspend fun mapToResultDomain(entitity: ResultEntity): Result{
        return entityMapper.mapToDomainModel(entitity)
    }


}