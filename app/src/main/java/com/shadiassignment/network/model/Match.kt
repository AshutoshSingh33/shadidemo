package com.shadiassignment.network.model

import com.google.gson.annotations.SerializedName

data class MatchResponseDTO(
    @SerializedName("results") val results: ArrayList<ResultDTO>,
    @SerializedName("info") val infoDTO: InfoDTO
)

data class ResultDTO(
    @SerializedName("gender") val gender: String,
    @SerializedName("name") val nameDTO: NameDTO,
    @SerializedName("location") val locationDTO: LocationDTO,
    @SerializedName("email") val email: String,
    @SerializedName("login") val loginDTO: LoginDTO,
    @SerializedName("dob") val dobDTO: DobDTO,
    @SerializedName("registered") val registeredDTO: RegisteredDTO,
    @SerializedName("phone") val phone: String,
    @SerializedName("cell") val cell: String,
    @SerializedName("id") val idDTO: IdDTO,
    @SerializedName("picture") val pictureDTO: PictureDTO,
    @SerializedName("nat") val nat: String
)

data class NameDTO(
    @SerializedName("title") val title: String,
    @SerializedName("first") val first: String,
    @SerializedName("last") val last: String
)

data class LocationDTO(
    @SerializedName("street") val streetDTO: StreetDTO,
    @SerializedName("city") val city: String,
    @SerializedName("state") val state: String,
    @SerializedName("country") val country: String,
    @SerializedName("postcode") val postcode: String,
    @SerializedName("coordinates") val coordinateDTO: CoordinateDTO,
    @SerializedName("timezone") val timeZoneDTO: TimeZoneDTO
)

data class StreetDTO(
    @SerializedName("number") val number: Int,
    @SerializedName("name") val name: String
)

data class CoordinateDTO(
    @SerializedName("latitude") val latitude: String,
    @SerializedName("longitude") val longitude: String
)

data class TimeZoneDTO(
    @SerializedName("offset") val offset: String,
    @SerializedName("description") val description: String
)

data class LoginDTO(
    @SerializedName("uuid") val uuid: String,
    @SerializedName("username") val username: String,
    @SerializedName("password") val password: String,
    @SerializedName("salt") val salt: String,
    @SerializedName("md5") val md5: String,
    @SerializedName("sha1") val sha1: String,
    @SerializedName("sha256") val sha256: String
)

data class DobDTO(
    @SerializedName("date") val date: String,
    @SerializedName("age") val age: Int
)

data class RegisteredDTO(
    @SerializedName("date") val date: String,
    @SerializedName("age") val age: Int
)

data class IdDTO(
    @SerializedName("name") val name: String?,
    @SerializedName("value") val value: String?
)

data class PictureDTO(
    @SerializedName("large") val large: String,
    @SerializedName("medium") val medium: String,
    @SerializedName("thumbnail") val thumbnail: String
)
data class InfoDTO(
    @SerializedName("seed") val seed: String,
    @SerializedName("results") val results: Int,
    @SerializedName("page") val page: Int,
    @SerializedName("version") val version: String
)