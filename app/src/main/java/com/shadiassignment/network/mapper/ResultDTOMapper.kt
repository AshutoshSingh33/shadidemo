package com.shadiassignment.network.mapper

import com.shadiassignment.database.model.ResultEntity
import com.shadiassignment.network.model.ResultDTO
import com.shadiassignment.presentation.match_result.ActionStatus
import com.shadiassignment.util.DomainMapper

class ResultDTOMapper : DomainMapper<ResultDTO, ResultEntity> {

    override fun mapToDomainModel(model: ResultDTO): ResultEntity {
        return ResultEntity(
            cell = model.cell,
            phone = model.phone,
            gender = model.gender,
            name = model.nameDTO,
            locationDTO = model.locationDTO,
            email = model.email,
            loginDTO = model.loginDTO,
            dobDTO = model.dobDTO,
            idDTO = model.idDTO,
            pictureDTO = model.pictureDTO,
            nat = model.nat,
            actionStatus = 0
        )
    }

    override fun mapFromDomainModel(domainModel: ResultEntity): ResultDTO {
        TODO("Not yet implemented")
    }

    fun toDomainList(initial: List<ResultDTO>): List<ResultEntity> {
        return initial.map { mapToDomainModel(it) }
    }

    fun fromDomainList(initial: List<ResultEntity>): List<ResultDTO> {
        return initial.map { mapFromDomainModel(it) }
    }

}