package com.shadiassignment.network


import com.shadiassignment.network.model.MatchResponseDTO
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface RetrofitService {

    @GET("api")
    suspend fun getMatchResult(@Query("results") results: String): MatchResponseDTO
}