package com.shadiassignment.presentation.match_result

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.shadiassignment.database.model.ResultEntity
import com.shadiassignment.databinding.ItemMatchesBinding
import com.shadiassignment.domain.Result


class MatchResultAdapter(
    val context: Context,
    var matches: ArrayList<Result>,
    var interaction: Interaction) :
    RecyclerView.Adapter<MatchResultAdapter.ResultViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ResultViewHolder {
        val view: ItemMatchesBinding =
            ItemMatchesBinding.inflate(
                LayoutInflater.from(context),
                parent,
                false
            )
        return ResultViewHolder(view, context, interaction)
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    override fun onBindViewHolder(holder: ResultViewHolder, position: Int) {
        holder.bindData(differ.currentList[position])
    }

    inner class ResultViewHolder(
        val itemMatchesBinding: ItemMatchesBinding,
        val context: Context,
        interaction: Interaction
    ) : RecyclerView.ViewHolder(itemMatchesBinding.root) {

        fun bindData(result: Result) {
            Glide.with(context).load(result.image).into(itemMatchesBinding.imageUser)

            itemMatchesBinding.textName.text = result.name
            itemMatchesBinding.textAge.text = result.age.toString()
            itemMatchesBinding.textDelhiNcr.text = "${result.city},  ${result.state} "

            result.actionStatus.also {
                if (it ==  ActionStatus.NONE) {
                    itemMatchesBinding.textConnect.text = "Connect Now"
                    itemMatchesBinding.linearLike.visibility = View.VISIBLE
                    itemMatchesBinding.linearCross.visibility = View.VISIBLE
                }  else if (it ==  ActionStatus.SENT) {
                    itemMatchesBinding.linearLike.visibility = View.GONE
                    itemMatchesBinding.linearCross.visibility = View.GONE
                    itemMatchesBinding.textConnect.text = "Sent"
                } else {
                    itemMatchesBinding.linearLike.visibility = View.GONE
                    itemMatchesBinding.linearCross.visibility = View.GONE
                    itemMatchesBinding.textConnect.text = "Rejected"
                }
            }

            itemMatchesBinding.linearLike.setOnClickListener {
                interaction.onItemTapped(differ.currentList[adapterPosition].cell, ActionStatus.SENT, adapterPosition)
            }

            itemMatchesBinding.linearCross.setOnClickListener {
                interaction.onItemTapped(differ.currentList[adapterPosition].cell, ActionStatus.REJECTED, adapterPosition)
            }
        }
    }

    interface Interaction {
        fun onItemTapped(cell: String, actionStatus: ActionStatus, position: Int)
    }

    val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Result>() {

        override fun areItemsTheSame(oldItem: Result, newItem: Result): Boolean {
            return oldItem.cell == newItem.cell
        }

        override fun areContentsTheSame(oldItem: Result, newItem: Result): Boolean {
            return oldItem == newItem
        }
    }

    private val differ = AsyncListDiffer(this, DIFF_CALLBACK)

    fun submitList(list: List<Result>) {
        differ.submitList(list.toMutableList())
    }
}