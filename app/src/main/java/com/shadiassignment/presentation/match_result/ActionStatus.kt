package com.shadiassignment.presentation.match_result

enum class ActionStatus(val value: Int){
    NONE(0),
    SENT(1),
    REJECTED(2)
}

fun getAllActionStatus(): List<ActionStatus>{
    return listOf(ActionStatus.NONE, ActionStatus.SENT, ActionStatus.REJECTED)
}

fun getActionStatus(value: Int): ActionStatus? {
    val map = ActionStatus.values().associateBy(ActionStatus::value)
    return map[value]
}