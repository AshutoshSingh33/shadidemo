package com.shadiassignment.presentation.match_result

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.shadiassignment.domain.Result
import com.shadiassignment.network.model.MatchResponseDTO
import com.shadiassignment.repository.MatchResultRepository
import kotlinx.coroutines.launch
import java.lang.Exception

class MatchResultListViewModel @ViewModelInject constructor(private val repository: MatchResultRepository) :
    ViewModel() {

    private var resultsLiveData: MutableLiveData<ArrayList<Result>> = MutableLiveData()
    private var loadingLiveData: MutableLiveData<Boolean> = MutableLiveData()

    init {
        onTriggerEvent(MatchResultEvent.GetMatchResultEvent)
    }

    fun onTriggerEvent(event: MatchResultEvent) {
        viewModelScope.launch {
            when (event) {
                is MatchResultEvent.GetMatchResultEvent -> getAllMatch()

                is MatchResultEvent.UpdateMatchEvent ->
                    updateMatch(event.cell, event.position, event.actionStatus)
            }
        }
    }

    private suspend fun getAllMatch() {
        try {
            loadingLiveData.value = true
            val matchResponseDTO: MatchResponseDTO = repository.getAllMatch("10")
            val matchResultEntiies = repository.mapToResultEntity(matchResponseDTO.results)
            resultsLiveData.value = repository.mapToResultDomainList(matchResultEntiies)
            loadingLiveData.value = false
        } catch (e: Exception) {
            loadingLiveData.value = false
            resultsLiveData.value = repository.mapToResultDomainList(repository.getAllResultEntities())
        }
    }

    fun getUserDataLiveData(): LiveData<ArrayList<Result>> {
        return resultsLiveData
    }

    fun getLoadingLiveData(): LiveData<Boolean> {
        return loadingLiveData
    }

    private suspend fun updateMatch(cell: String, position: Int, actionStatus: ActionStatus) {
        val updatedResultEntity = repository.updateResultEntity(cell, actionStatus)
        resultsLiveData.value?.set(position, repository.mapToResultDomain(updatedResultEntity))
        resultsLiveData.value = resultsLiveData.value
    }

    sealed class MatchResultEvent {
        object GetMatchResultEvent : MatchResultEvent()
        data class UpdateMatchEvent(val cell: String, val position: Int, val actionStatus: ActionStatus) : MatchResultEvent()

    }
}