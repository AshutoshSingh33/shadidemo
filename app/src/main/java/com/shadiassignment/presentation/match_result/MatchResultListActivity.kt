package com.shadiassignment.presentation.match_result

import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.shadiassignment.R
import com.shadiassignment.database.model.ResultEntity
import com.shadiassignment.databinding.ActivityMainBinding
import com.shadiassignment.domain.Result
import dagger.hilt.android.AndroidEntryPoint
import com.shadiassignment.presentation.match_result.MatchResultListViewModel.MatchResultEvent

@AndroidEntryPoint
class MatchResultListActivity : AppCompatActivity(), MatchResultAdapter.Interaction {

    private lateinit var matchResultAdapter: MatchResultAdapter
    private val viewModel: MatchResultListViewModel by viewModels()
    private lateinit var mainBinding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(mainBinding.root)
        setAdapter(arrayListOf())

        viewModel.getUserDataLiveData().observe(this, Observer {
            System.out.println(it)
            matchResultAdapter.submitList(it)

        })

        viewModel.getLoadingLiveData().observe(this, Observer {
            if (it) mainBinding.progressBar.visibility = View.VISIBLE
            else mainBinding.progressBar.visibility = View.GONE
        })
    }

    fun setAdapter(matches: ArrayList<Result>) {
        matchResultAdapter = MatchResultAdapter(this, matches, this)
        mainBinding.recycleMatches.layoutManager = LinearLayoutManager(this)
        mainBinding.recycleMatches.adapter = matchResultAdapter
    }

    override fun onItemTapped(cell: String, actionStatus: ActionStatus, position: Int) {
       viewModel.onTriggerEvent(MatchResultEvent.UpdateMatchEvent(cell, position, actionStatus))
    }
}